﻿CREATE PROCEDURE [SYNTO].[API_ELEMENT_GLOBALPROPERTY_UPDATE]
	@ELEMENT_GLOBALPROPERTY_UID	UNIQUEIDENTIFIER
	,@MODIFIED_BY	NVARCHAR(150)
	,@ISAUTOCOLLECT	BIT
	,@UISORT_ORDER	INT
	,@ELEMENT_UID	UNIQUEIDENTIFIER
	,@TYPE_UID	UNIQUEIDENTIFIER
	,@GLOBAL_ATTRIBUTE	NVARCHAR(MAX)
	,@MODULE_UID_AUTOCOLLECT	UNIQUEIDENTIFIER
	,@RLS_OWNER UNIQUEIDENTIFIER
	,@RLS_MASK VARBINARY(MAX)
AS
BEGIN
	UPDATE [SYNTO].[ELEMENT_GLOBALPROPERTY]
	   SET [MODIFIED_BY] = @MODIFIED_BY
			,[ISAUTOCOLLECT] = @ISAUTOCOLLECT
			,[UISORT_ORDER] = @UISORT_ORDER
			,[ELEMENT_UID] = @ELEMENT_UID
			,[TYPE_UID] = @TYPE_UID
			,[GLOBAL_ATTRIBUTE] = @GLOBAL_ATTRIBUTE
			,[MODULE_UID_AUTOCOLLECT] = @MODULE_UID_AUTOCOLLECT
			,[RLS_OWNER] = @RLS_OWNER
			,[RLS_MASK] = @RLS_MASK
	 WHERE [ELEMENT_GLOBALPROPERTY_UID] = @ELEMENT_GLOBALPROPERTY_UID
END