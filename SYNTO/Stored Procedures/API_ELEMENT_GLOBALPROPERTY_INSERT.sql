﻿CREATE PROCEDURE [SYNTO].[API_ELEMENT_GLOBALPROPERTY_INSERT]
	@ELEMENT_GLOBALPROPERTY_PK	INT = NULL OUTPUT
	,@ELEMENT_GLOBALPROPERTY_UID UNIQUEIDENTIFIER = NULL OUTPUT
	,@MODIFIED_BY	NVARCHAR(150)
	,@ISAUTOCOLLECT	BIT
	,@UISORT_ORDER	INT
	,@ELEMENT_UID	UNIQUEIDENTIFIER
	,@TYPE_UID	UNIQUEIDENTIFIER
	,@GLOBAL_ATTRIBUTE	NVARCHAR(MAX)
	,@MODULE_UID_AUTOCOLLECT	UNIQUEIDENTIFIER
	,@RLS_OWNER UNIQUEIDENTIFIER
	,@RLS_MASK VARBINARY(MAX)

AS
BEGIN

	SELECT @ELEMENT_GLOBALPROPERTY_UID = NEWID() 
	
	INSERT INTO  [SYNTO].[ELEMENT_GLOBALPROPERTY]
	(
		 [ELEMENT_GLOBALPROPERTY_UID]
		,[MODIFIED_BY]
		,[ISAUTOCOLLECT]
		,[UISORT_ORDER]
		,[ELEMENT_UID]
		,[TYPE_UID]
		,[GLOBAL_ATTRIBUTE]
		,[MODULE_UID_AUTOCOLLECT]
		,[RLS_OWNER]
		,[RLS_MASK]
	)
	VALUES
	(
		@ELEMENT_GLOBALPROPERTY_UID
		,@MODIFIED_BY
		,@ISAUTOCOLLECT
		,@UISORT_ORDER
		,@ELEMENT_UID
		,@TYPE_UID
		,@GLOBAL_ATTRIBUTE
		,@MODULE_UID_AUTOCOLLECT
		,@RLS_OWNER
		,@RLS_MASK
	)

	SELECT @ELEMENT_GLOBALPROPERTY_PK = SCOPE_IDENTITY()
END