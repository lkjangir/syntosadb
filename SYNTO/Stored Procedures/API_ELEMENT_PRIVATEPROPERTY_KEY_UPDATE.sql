﻿CREATE PROCEDURE [SYNTO].[API_ELEMENT_PRIVATEPROPERTY_KEY_UPDATE]
	@ELEMENT_PRIVATEPROPERTY_KEY_UID	UNIQUEIDENTIFIER
	,@ELEMENT_PRIVATEPROPERTY_KEY_UID_PARENT	UNIQUEIDENTIFIER
	,@MODIFIED_BY	NVARCHAR(150)
	,@ISAUTOCOLLECT	BIT
	,@UISORT_ORDER	INT
	,@ELEMENT_UID	UNIQUEIDENTIFIER
	,@TYPE_UID_KEY	UNIQUEIDENTIFIER
	,@TYPE_UID_VALUE	UNIQUEIDENTIFIER
	,@TYPE_UNIT_UID UNIQUEIDENTIFIER
	,@MODULE_UID_AUTOCOLLECT	UNIQUEIDENTIFIER

AS
BEGIN
	UPDATE [SYNTO].[ELEMENT_PRIVATEPROPERTY_KEY]
	   SET [ELEMENT_PRIVATEPROPERTY_KEY_UID_PARENT] = @ELEMENT_PRIVATEPROPERTY_KEY_UID_PARENT
			,[MODIFIED_BY] = @MODIFIED_BY
			,[ISAUTOCOLLECT] = @ISAUTOCOLLECT
			,[UISORT_ORDER] = @UISORT_ORDER
			,[ELEMENT_UID] = @ELEMENT_UID
			,[TYPE_UID_KEY] = @TYPE_UID_KEY
			,[TYPE_UID_VALUE] = @TYPE_UID_VALUE
			,[TYPE_UNIT_UID] = @TYPE_UNIT_UID
			,[MODULE_UID_AUTOCOLLECT] = @MODULE_UID_AUTOCOLLECT
	 WHERE [ELEMENT_PRIVATEPROPERTY_KEY_UID] = @ELEMENT_PRIVATEPROPERTY_KEY_UID
END