﻿CREATE PROCEDURE [SYNTO].[API_ELEMENT_DIAGRAM_SELECT]
	 @ELEMENT_DIAGRAM_PK	INT = NULL
	 ,@ELEMENT_DIAGRAM_UID	UNIQUEIDENTIFIER = NULL
	 ,@MODIFIED_BY	NVARCHAR(150) = NULL
	 ,@ELEMENT_UID	UNIQUEIDENTIFIER = NULL
	 ,@ELEMENT_NAME	NVARCHAR(150) = NULL
	 ,@ISACTIVE_ELEMENT	BIT = NULL
AS
BEGIN
 
	SELECT	[ELEMENT_DIAGRAM_PK]
		    ,[ELEMENT_DIAGRAM_UID]
		    ,[ELEMENT_DIAGRAM_UID_PARENT]
		    ,[MODIFIED_BY]
		    ,[ELEMENT_UID]
		    ,[DIAGRAM]
		    ,[RLS_OWNER]
		    ,[RLS_MASK]
		    ,[SYS_START_TIME]
		    ,[SYS_END_TIME]
		    ,[ELEMENT_NAME]
		    ,[ISACTIVE_ELEMENT]
	  FROM	[SYNTO].[VW_ELEMENT_DIAGRAM]
	 WHERE	[ELEMENT_DIAGRAM_PK] = COALESCE(@ELEMENT_DIAGRAM_PK, [ELEMENT_DIAGRAM_PK])
	   AND	[ELEMENT_DIAGRAM_UID] = COALESCE(@ELEMENT_DIAGRAM_UID, [ELEMENT_DIAGRAM_UID])
	   AND	[MODIFIED_BY] = COALESCE(@MODIFIED_BY, [MODIFIED_BY])
	   AND	[ELEMENT_UID] = COALESCE(@ELEMENT_UID, [ELEMENT_UID])
	   AND	[ELEMENT_NAME] = COALESCE(@ELEMENT_NAME, [ELEMENT_NAME])
	   AND	[ISACTIVE_ELEMENT] = COALESCE(@ISACTIVE_ELEMENT, [ISACTIVE_ELEMENT])
END