﻿CREATE PROCEDURE [SYNTO].[API_ELEMENT_GLOBALPROPERTY_DELETE]
	@ELEMENT_GLOBALPROPERTY_UID	UNIQUEIDENTIFIER
AS
BEGIN 
	DELETE	[SYNTO].[ELEMENT_GLOBALPROPERTY]
	 WHERE	[ELEMENT_GLOBALPROPERTY_UID] = @ELEMENT_GLOBALPROPERTY_UID

END