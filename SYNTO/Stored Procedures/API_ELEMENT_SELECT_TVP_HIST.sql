﻿CREATE PROCEDURE [SYNTO].[API_ELEMENT_SELECT_TVP_HIST]
	 @ELEMENT_TVP AS [SYNTO].[ELEMENT_UDT] READONLY 
	,@SYSTEM_TIME DATETIME2
AS
BEGIN
 
	SELECT	 A.[ELEMENT_PK]
		    ,A.[ELEMENT_UID]
		    ,A.[ELEMENT_UID_PARENT]
		    ,A.[MODIFIED_BY]
		    ,A.[ISBUILTIN]
		    ,A.[ISACTIVE]
		    ,A.[ISAUTOCOLLECT]
		    ,A.[MODULE_UID]
		    ,A.[MODULE_RECORD_PK]
		    ,A.[TYPE_UID_RECORD_STATUS]
		    ,A.[DOMAIN_UID]
		    ,A.[TYPE_UID]
		    ,A.[ELEMENT_NAME]
		    ,A.[ELEMENT_ALIAS]
		    ,A.[ELEMENT_DESC]
		    ,A.[RLS_OWNER]
		    ,A.[RLS_MASK]
		    ,A.[SYS_START_TIME]
		    ,A.[SYS_END_TIME]
		    ,A.[MODULE_NAME]
		    ,A.[ISACTIVE_MODULE]
		    ,A.[DOMAIN_NAME]
		    ,A.[ISACTIVE_DOMAIN]
		    ,A.[TYPE_FUNCTION_NAME]
		    ,A.[ISACTIVE_TYPE_FUNCTION]
		    ,A.[TYPE_NAME]
		    ,A.[ISACTIVE_TYPE]
		    ,A.[RECORD_STATUS]
		    ,A.[ELEMENT_NAME_PARENT]
		    ,A.[ISACTIVE_ELEMENT_PARENT]
	  FROM	[SYNTO].[VW_ELEMENT] FOR SYSTEM_TIME AS OF @SYSTEM_TIME A
     WHERE	 A.[TYPE_UID] IN (SELECT [TYPE_UID] FROM @ELEMENT_TVP)
END
