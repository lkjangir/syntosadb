﻿CREATE PROCEDURE [SYNTO].[API_ELEMENT_SELECT_HIERALL]
	@ELEMENT_UID	UNIQUEIDENTIFIER
AS
BEGIN
 
	DECLARE @TMP_HIERALL TABLE 
	(
	 [ELEMENT_PK]	INT NULL
	 ,[ELEMENT_UID]	UNIQUEIDENTIFIER NULL
	 ,[ELEMENT_UID_PARENT]	UNIQUEIDENTIFIER NULL
	 ,[MODIFIED_BY]	NVARCHAR(150) NULL
	 ,[ISBUILTIN]	BIT NULL
	 ,[ISACTIVE]	BIT NULL
	 ,[ISAUTOCOLLECT]	BIT NULL
	 ,[MODULE_UID]	UNIQUEIDENTIFIER NULL
	 ,[MODULE_RECORD_PK]	NVARCHAR(250) NULL
	 ,[TYPE_UID_RECORD_STATUS]	UNIQUEIDENTIFIER NULL
	 ,[DOMAIN_UID]	UNIQUEIDENTIFIER NULL
	 ,[TYPE_UID]	UNIQUEIDENTIFIER NULL
	 ,[ELEMENT_NAME]	NVARCHAR(150) NULL
	 ,[ELEMENT_ALIAS]	NVARCHAR(150) NULL
	 ,[ELEMENT_DESC]	NVARCHAR(MAX) NULL
	 ,[RLS_OWNER]	UNIQUEIDENTIFIER NULL
	 ,[RLS_MASK]	VARBINARY(MAX) NULL
	 ,[SYS_START_TIME]	DATETIME2 NULL
	 ,[SYS_END_TIME]	DATETIME2 NULL
	 ,[MODULE_NAME]	NVARCHAR(150) NULL
	 ,[ISACTIVE_MODULE]	BIT NULL
	 ,[DOMAIN_NAME]	NVARCHAR(150) NULL
	 ,[ISACTIVE_DOMAIN]	BIT NULL
	 ,[TYPE_FUNCTION_NAME]	NVARCHAR(150) NULL
	 ,[ISACTIVE_TYPE_FUNCTION]	BIT NULL
	 ,[TYPE_NAME]	NVARCHAR(150) NULL
	 ,[ISACTIVE_TYPE]	BIT NULL
	 ,[RECORD_STATUS]	NVARCHAR(150) NULL
	 ,[ELEMENT_NAME_PARENT]	NVARCHAR(150) NULL
	 ,[ISACTIVE_ELEMENT_PARENT]	BIT NULL
	)
 
	BEGIN
		-- WALK UP THE TREE.
		WITH [NESTED_UP] AS
		(
			SELECT	[ELEMENT_PK]
					,[ELEMENT_UID]
					,[ELEMENT_UID_PARENT]
					,[MODIFIED_BY]
					,[ISBUILTIN]
					,[ISACTIVE]
					,[ISAUTOCOLLECT]
					,[MODULE_UID]
					,[MODULE_RECORD_PK]
					,[TYPE_UID_RECORD_STATUS]
					,[DOMAIN_UID]
					,[TYPE_UID]
					,[ELEMENT_NAME]
					,[ELEMENT_ALIAS]
					,[ELEMENT_DESC]
					,[RLS_OWNER]
					,[RLS_MASK]
					,[SYS_START_TIME]
					,[SYS_END_TIME]
					,[MODULE_NAME]
					,[ISACTIVE_MODULE]
					,[DOMAIN_NAME]
					,[ISACTIVE_DOMAIN]
					,[TYPE_FUNCTION_NAME]
					,[ISACTIVE_TYPE_FUNCTION]
					,[TYPE_NAME]
					,[ISACTIVE_TYPE]
					,[RECORD_STATUS]
					,[ELEMENT_NAME_PARENT]
					,[ISACTIVE_ELEMENT_PARENT]
			FROM [SYNTO].[VW_ELEMENT]
			WHERE [ELEMENT_UID] = @ELEMENT_UID
 
			UNION ALL
 
			SELECT	B.[ELEMENT_PK]
					,B.[ELEMENT_UID]
					,B.[ELEMENT_UID_PARENT]
					,B.[MODIFIED_BY]
					,B.[ISBUILTIN]
					,B.[ISACTIVE]
					,B.[ISAUTOCOLLECT]
					,B.[MODULE_UID]
					,B.[MODULE_RECORD_PK]
					,B.[TYPE_UID_RECORD_STATUS]
					,B.[DOMAIN_UID]
					,B.[TYPE_UID]
					,B.[ELEMENT_NAME]
					,B.[ELEMENT_ALIAS]
					,B.[ELEMENT_DESC]
					,B.[RLS_OWNER]
					,B.[RLS_MASK]
					,B.[SYS_START_TIME]
					,B.[SYS_END_TIME]
					,B.[MODULE_NAME]
					,B.[ISACTIVE_MODULE]
					,B.[DOMAIN_NAME]
					,B.[ISACTIVE_DOMAIN]
					,B.[TYPE_FUNCTION_NAME]
					,B.[ISACTIVE_TYPE_FUNCTION]
					,B.[TYPE_NAME]
					,B.[ISACTIVE_TYPE]
					,B.[RECORD_STATUS]
					,B.[ELEMENT_NAME_PARENT]
					,B.[ISACTIVE_ELEMENT_PARENT]
			  FROM [NESTED_UP] A
			 INNER JOIN [SYNTO].[VW_ELEMENT] B
				ON  A.[ELEMENT_UID_PARENT] = B.[ELEMENT_UID]
		)
		INSERT INTO @TMP_HIERALL
		(
			[ELEMENT_PK]
			,[ELEMENT_UID]
			,[ELEMENT_UID_PARENT]
			,[MODIFIED_BY]
			,[ISBUILTIN]
			,[ISACTIVE]
			,[ISAUTOCOLLECT]
			,[MODULE_UID]
			,[MODULE_RECORD_PK]
			,[TYPE_UID_RECORD_STATUS]
			,[DOMAIN_UID]
			,[TYPE_UID]
			,[ELEMENT_NAME]
			,[ELEMENT_ALIAS]
			,[ELEMENT_DESC]
			,[RLS_OWNER]
			,[RLS_MASK]
			,[SYS_START_TIME]
			,[SYS_END_TIME]
			,[MODULE_NAME]
			,[ISACTIVE_MODULE]
			,[DOMAIN_NAME]
			,[ISACTIVE_DOMAIN]
			,[TYPE_FUNCTION_NAME]
			,[ISACTIVE_TYPE_FUNCTION]
			,[TYPE_NAME]
			,[ISACTIVE_TYPE]
			,[RECORD_STATUS]
			,[ELEMENT_NAME_PARENT]
			,[ISACTIVE_ELEMENT_PARENT]
		)
		SELECT	[ELEMENT_PK]
				,[ELEMENT_UID]
				,[ELEMENT_UID_PARENT]
				,[MODIFIED_BY]
				,[ISBUILTIN]
				,[ISACTIVE]
				,[ISAUTOCOLLECT]
				,[MODULE_UID]
				,[MODULE_RECORD_PK]
				,[TYPE_UID_RECORD_STATUS]
				,[DOMAIN_UID]
				,[TYPE_UID]
				,[ELEMENT_NAME]
				,[ELEMENT_ALIAS]
				,[ELEMENT_DESC]
				,[RLS_OWNER]
				,[RLS_MASK]
				,[SYS_START_TIME]
				,[SYS_END_TIME]
				,[MODULE_NAME]
				,[ISACTIVE_MODULE]
				,[DOMAIN_NAME]
				,[ISACTIVE_DOMAIN]
				,[TYPE_FUNCTION_NAME]
				,[ISACTIVE_TYPE_FUNCTION]
				,[TYPE_NAME]
				,[ISACTIVE_TYPE]
				,[RECORD_STATUS]
				,[ELEMENT_NAME_PARENT]
				,[ISACTIVE_ELEMENT_PARENT]
 
		 FROM [NESTED_UP];
		-- WALK DOWN THE TREE.
		WITH [NESTED_DOWN] AS
		(
			SELECT	[ELEMENT_PK]
					,[ELEMENT_UID]
					,[ELEMENT_UID_PARENT]
					,[MODIFIED_BY]
					,[ISBUILTIN]
					,[ISACTIVE]
					,[ISAUTOCOLLECT]
					,[MODULE_UID]
					,[MODULE_RECORD_PK]
					,[TYPE_UID_RECORD_STATUS]
					,[DOMAIN_UID]
					,[TYPE_UID]
					,[ELEMENT_NAME]
					,[ELEMENT_ALIAS]
					,[ELEMENT_DESC]
					,[RLS_OWNER]
					,[RLS_MASK]
					,[SYS_START_TIME]
					,[SYS_END_TIME]
					,[MODULE_NAME]
					,[ISACTIVE_MODULE]
					,[DOMAIN_NAME]
					,[ISACTIVE_DOMAIN]
					,[TYPE_FUNCTION_NAME]
					,[ISACTIVE_TYPE_FUNCTION]
					,[TYPE_NAME]
					,[ISACTIVE_TYPE]
					,[RECORD_STATUS]
					,[ELEMENT_NAME_PARENT]
					,[ISACTIVE_ELEMENT_PARENT]
			FROM [SYNTO].[VW_ELEMENT]
			WHERE [ELEMENT_UID] = @ELEMENT_UID
 
			UNION ALL
 
			SELECT	B.[ELEMENT_PK]
					,B.[ELEMENT_UID]
					,B.[ELEMENT_UID_PARENT]
					,B.[MODIFIED_BY]
					,B.[ISBUILTIN]
					,B.[ISACTIVE]
					,B.[ISAUTOCOLLECT]
					,B.[MODULE_UID]
					,B.[MODULE_RECORD_PK]
					,B.[TYPE_UID_RECORD_STATUS]
					,B.[DOMAIN_UID]
					,B.[TYPE_UID]
					,B.[ELEMENT_NAME]
					,B.[ELEMENT_ALIAS]
					,B.[ELEMENT_DESC]
					,B.[RLS_OWNER]
					,B.[RLS_MASK]
					,B.[SYS_START_TIME]
					,B.[SYS_END_TIME]
					,B.[MODULE_NAME]
					,B.[ISACTIVE_MODULE]
					,B.[DOMAIN_NAME]
					,B.[ISACTIVE_DOMAIN]
					,B.[TYPE_FUNCTION_NAME]
					,B.[ISACTIVE_TYPE_FUNCTION]
					,B.[TYPE_NAME]
					,B.[ISACTIVE_TYPE]
					,B.[RECORD_STATUS]
					,B.[ELEMENT_NAME_PARENT]
					,B.[ISACTIVE_ELEMENT_PARENT]
			  FROM [NESTED_DOWN] A
			 INNER JOIN [SYNTO].[VW_ELEMENT] B
				ON A.[ELEMENT_UID] = B.[ELEMENT_UID_PARENT]
		)
		INSERT INTO @TMP_HIERALL
		(
			[ELEMENT_PK]
			,[ELEMENT_UID]
			,[ELEMENT_UID_PARENT]
			,[MODIFIED_BY]
			,[ISBUILTIN]
			,[ISACTIVE]
			,[ISAUTOCOLLECT]
			,[MODULE_UID]
			,[MODULE_RECORD_PK]
			,[TYPE_UID_RECORD_STATUS]
			,[DOMAIN_UID]
			,[TYPE_UID]
			,[ELEMENT_NAME]
			,[ELEMENT_ALIAS]
			,[ELEMENT_DESC]
			,[RLS_OWNER]
			,[RLS_MASK]
			,[SYS_START_TIME]
			,[SYS_END_TIME]
			,[MODULE_NAME]
			,[ISACTIVE_MODULE]
			,[DOMAIN_NAME]
			,[ISACTIVE_DOMAIN]
			,[TYPE_FUNCTION_NAME]
			,[ISACTIVE_TYPE_FUNCTION]
			,[TYPE_NAME]
			,[ISACTIVE_TYPE]
			,[RECORD_STATUS]
			,[ELEMENT_NAME_PARENT]
			,[ISACTIVE_ELEMENT_PARENT]
		)
		SELECT	[ELEMENT_PK]
			,[ELEMENT_UID]
			,[ELEMENT_UID_PARENT]
			,[MODIFIED_BY]
			,[ISBUILTIN]
			,[ISACTIVE]
			,[ISAUTOCOLLECT]
			,[MODULE_UID]
			,[MODULE_RECORD_PK]
			,[TYPE_UID_RECORD_STATUS]
			,[DOMAIN_UID]
			,[TYPE_UID]
			,[ELEMENT_NAME]
			,[ELEMENT_ALIAS]
			,[ELEMENT_DESC]
			,[RLS_OWNER]
			,[RLS_MASK]
			,[SYS_START_TIME]
			,[SYS_END_TIME]
			,[MODULE_NAME]
			,[ISACTIVE_MODULE]
			,[DOMAIN_NAME]
			,[ISACTIVE_DOMAIN]
			,[TYPE_FUNCTION_NAME]
			,[ISACTIVE_TYPE_FUNCTION]
			,[TYPE_NAME]
			,[ISACTIVE_TYPE]
			,[RECORD_STATUS]
			,[ELEMENT_NAME_PARENT]
			,[ISACTIVE_ELEMENT_PARENT]
		FROM [NESTED_DOWN]
 
		-- RETURN FULL HEIRARCHY
		SELECT DISTINCT * 
		  FROM @TMP_HIERALL
		 ORDER BY [ELEMENT_UID_PARENT]
	END
END