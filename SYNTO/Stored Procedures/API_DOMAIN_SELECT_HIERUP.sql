﻿CREATE PROCEDURE [SYNTO].[API_DOMAIN_SELECT_HIERUP]
	@DOMAIN_UID	UNIQUEIDENTIFIER
AS
BEGIN
 
	-- WALK UP THE TREE.
	WITH [NESTED_UP] AS
	(
		SELECT	[DOMAIN_PK]
				,[DOMAIN_UID]
				,[MODIFIED_BY]
				,[DOMAIN_UID_PARENT]
				,[ISACTIVE]
				,[ISBUILTIN]
				,[DOMAIN_NAME]
				,[DOMAIN_DESC]
				,[RLS_OWNER]
				,[RLS_MASK]
				,[SYS_START_TIME]
				,[SYS_END_TIME]
				,[DOMAIN_NAME_PARENT]
				,[ISACTIVE_DOMAIN_PARENT]
		  FROM [SYNTO].[VW_DOMAIN]
		 WHERE [DOMAIN_UID] = @DOMAIN_UID
 
		UNION ALL
 
		SELECT	B.[DOMAIN_PK]
				,B.[DOMAIN_UID]
				,B.[MODIFIED_BY]
				,B.[DOMAIN_UID_PARENT]
				,B.[ISACTIVE]
				,B.[ISBUILTIN]
				,B.[DOMAIN_NAME]
				,B.[DOMAIN_DESC]
				,B.[RLS_OWNER]
				,B.[RLS_MASK]
				,B.[SYS_START_TIME]
				,B.[SYS_END_TIME]
				,B.[DOMAIN_NAME_PARENT]
				,B.[ISACTIVE_DOMAIN_PARENT]
		  FROM [NESTED_UP] A
		 INNER JOIN [SYNTO].[VW_DOMAIN] B
			ON  A.[DOMAIN_UID_PARENT] = B.[DOMAIN_UID]
	)
	SELECT	[DOMAIN_PK]
			,[DOMAIN_UID]
			,[MODIFIED_BY]
			,[DOMAIN_UID_PARENT]
			,[ISACTIVE]
			,[ISBUILTIN]
			,[DOMAIN_NAME]
			,[DOMAIN_DESC]
			,[RLS_OWNER]
			,[RLS_MASK]
			,[SYS_START_TIME]
			,[SYS_END_TIME]
			,[DOMAIN_NAME_PARENT]
			,[ISACTIVE_DOMAIN_PARENT]
	 FROM [NESTED_UP]
	ORDER BY [DOMAIN_UID_PARENT]
END