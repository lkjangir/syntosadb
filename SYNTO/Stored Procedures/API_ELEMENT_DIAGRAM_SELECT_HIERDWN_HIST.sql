﻿CREATE PROCEDURE [SYNTO].[API_ELEMENT_DIAGRAM_SELECT_HIERDWN_HIST]
	@ELEMENT_DIAGRAM_UID	UNIQUEIDENTIFIER
	,@SYSTEM_TIME DATETIME2
AS
BEGIN
 
	-- WALK DOWN THE TREE.
	WITH [NESTED_DOWN] AS
	(
		SELECT	[ELEMENT_DIAGRAM_PK]
				,[ELEMENT_DIAGRAM_UID]
				,[ELEMENT_DIAGRAM_UID_PARENT]
				,[MODIFIED_BY]
				,[ELEMENT_UID]
				,[RLS_OWNER]
				,[RLS_MASK]
				,[SYS_START_TIME]
				,[SYS_END_TIME]
				,[ELEMENT_NAME]
				,[ISACTIVE_ELEMENT]
		  FROM [SYNTO].[VW_ELEMENT_DIAGRAM] FOR SYSTEM_TIME AS OF @SYSTEM_TIME
		 WHERE [ELEMENT_DIAGRAM_UID] = @ELEMENT_DIAGRAM_UID
 
		UNION ALL
 
		SELECT	B.[ELEMENT_DIAGRAM_PK]
				,B.[ELEMENT_DIAGRAM_UID]
				,B.[ELEMENT_DIAGRAM_UID_PARENT]
				,B.[MODIFIED_BY]
				,B.[ELEMENT_UID]
				,B.[RLS_OWNER]
				,B.[RLS_MASK]
				,B.[SYS_START_TIME]
				,B.[SYS_END_TIME]
				,B.[ELEMENT_NAME]
				,B.[ISACTIVE_ELEMENT]
		  FROM [NESTED_DOWN] A
		 INNER JOIN [SYNTO].[VW_ELEMENT_DIAGRAM] FOR SYSTEM_TIME AS OF @SYSTEM_TIME B
			ON  A.[ELEMENT_DIAGRAM_UID] = B.[ELEMENT_DIAGRAM_UID_PARENT]
	)
	SELECT	[ELEMENT_DIAGRAM_PK]
			,[ELEMENT_DIAGRAM_UID]
			,[ELEMENT_DIAGRAM_UID_PARENT]
			,[MODIFIED_BY]
			,[ELEMENT_UID]
			,[RLS_OWNER]
			,[RLS_MASK]
			,[SYS_START_TIME]
			,[SYS_END_TIME]
			,[ELEMENT_NAME]
			,[ISACTIVE_ELEMENT]
	 FROM [NESTED_DOWN]
	ORDER BY [ELEMENT_DIAGRAM_UID_PARENT]
END