﻿CREATE TABLE [SYNTO].[ELEMENT_DIAGRAM]
(
	[ELEMENT_DIAGRAM_PK] INT NOT NULL IDENTITY,
	[ELEMENT_DIAGRAM_UID] UNIQUEIDENTIFIER NOT NULL,
    [ELEMENT_DIAGRAM_UID_PARENT] UNIQUEIDENTIFIER NULL,
	[MODIFIED_BY] NVARCHAR(150) NOT NULL,
	[ELEMENT_UID] UNIQUEIDENTIFIER NOT NULL,
	[DIAGRAM] XML NOT NULL,
	[RLS_OWNER] UNIQUEIDENTIFIER NULL,
	[RLS_MASK] VARBINARY(MAX) NULL,
	[SYS_START_TIME] DATETIME2 GENERATED ALWAYS AS ROW START NOT NULL DEFAULT (GETUTCDATE()),
	[SYS_END_TIME] DATETIME2 GENERATED ALWAYS AS ROW END NOT NULL DEFAULT (CONVERT([DATETIME2], '9999-12-31 23:59:59.9999999')),
	PERIOD FOR SYSTEM_TIME ([SYS_START_TIME],[SYS_END_TIME]),
    CONSTRAINT [PK_ELEMENT_DIAGRAM] PRIMARY KEY ([ELEMENT_DIAGRAM_PK]),
 	CONSTRAINT [FK_ELEMENT_DIAGRAM_ELEMENT_UID] FOREIGN KEY ([ELEMENT_UID]) REFERENCES [SYNTO].[ELEMENT]([ELEMENT_UID]),
	CONSTRAINT [FK_ELEMENT_DIAGRAM_ELEMENT_DIAGRAM_UID_PARENT] FOREIGN KEY ([ELEMENT_DIAGRAM_UID_PARENT]) REFERENCES [SYNTO].[ELEMENT_DIAGRAM]([ELEMENT_DIAGRAM_UID]),

)
WITH  
   ( 
      SYSTEM_VERSIONING = ON (HISTORY_TABLE = [SYNTO].[HIST_ELEMENT_DIAGRAM])
	  ,DATA_COMPRESSION = PAGE
   );
GO
CREATE UNIQUE INDEX [UK_ELEMENT_DIAGRAM_UID] ON [SYNTO].[ELEMENT_DIAGRAM] ([ELEMENT_DIAGRAM_UID])
GO
CREATE INDEX [IX_ELEMENT_DIAGRAM_ELEMENT_UID_PARENT] ON [SYNTO].[ELEMENT_DIAGRAM] ([ELEMENT_DIAGRAM_UID_PARENT])
GO
CREATE INDEX [IX_ELEMENT_DIAGRAM_ELEMENT_UID] ON [SYNTO].[ELEMENT_DIAGRAM] ([ELEMENT_UID])
GO