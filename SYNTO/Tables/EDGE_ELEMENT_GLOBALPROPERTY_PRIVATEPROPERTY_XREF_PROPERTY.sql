﻿CREATE TABLE [SYNTO].[EDGE_ELEMENT_GLOBALPROPERTY_PRIVATEPROPERTY_XREF_PROPERTY] 
(
    [EDGE_ELEMENT_GLOBALPROPERTY_PRIVATEPROPERTY_XREF_PROPERTY_PK]  INT NOT NULL IDENTITY,
    [EDGE_ELEMENT_GLOBALPROPERTY_PRIVATEPROPERTY_XREF_PROPERTY_UID] UNIQUEIDENTIFIER NOT NULL,
    [MODIFIED_BY] NVARCHAR (150) NOT NULL,
    [TYPE_UID_EDGE_PROPERTY] UNIQUEIDENTIFIER NOT NULL,
    [EDGE_ELEMENT_GLOBALPROPERTY_PRIVATEPROPERTY_XREF_UID] UNIQUEIDENTIFIER NOT NULL,
    [EDGE_PROPERTY_ATTRIBUTE] VARCHAR (MAX) NULL,
    [SYS_START_TIME] DATETIME2 (7) GENERATED ALWAYS AS ROW START DEFAULT (getutcdate()) NOT NULL,
    [SYS_END_TIME] DATETIME2 (7) GENERATED ALWAYS AS ROW END   DEFAULT (CONVERT([datetime2],'9999-12-31 23:59:59.9999999')) NOT NULL,
    CONSTRAINT [PK_EDGE_ELEMENT_GLOBALPROPERTY_PRIVATEPROPERTY_XREF_PROPERTY] PRIMARY KEY CLUSTERED ([EDGE_ELEMENT_GLOBALPROPERTY_PRIVATEPROPERTY_XREF_PROPERTY_PK] ASC) WITH (DATA_COMPRESSION = PAGE),
    CONSTRAINT [FK_EDGE_ELEMENT_GLOBALPROPERTY_PRIVATEPROPERTY_XREF_PROPERTY_TYPE_UID] FOREIGN KEY ([TYPE_UID_EDGE_PROPERTY]) REFERENCES [SYNTO].[TYPE] ([TYPE_UID]),
    CONSTRAINT [FK_EDGE_ELEMENT_GLOBALPROPERTY_PRIVATEPROPERTY_XREF_PROPERTY_UID] FOREIGN KEY ([EDGE_ELEMENT_GLOBALPROPERTY_PRIVATEPROPERTY_XREF_UID]) REFERENCES [SYNTO].[EDGE_ELEMENT_GLOBALPROPERTY_PRIVATEPROPERTY_XREF] ([EDGE_ELEMENT_GLOBALPROPERTY_PRIVATEPROPERTY_XREF_UID]),
    PERIOD FOR SYSTEM_TIME ([SYS_START_TIME], [SYS_END_TIME])
)
WITH 
    (
        SYSTEM_VERSIONING = ON (HISTORY_TABLE=[SYNTO].[HIST_EDGE_ELEMENT_GLOBALPROPERTY_PRIVATEPROPERTY_XREF_PROPERTY], 
        DATA_CONSISTENCY_CHECK=ON)
    );
GO
CREATE UNIQUE INDEX [UX_EDGE_ELEMENT_GLOBALPROPERTY_PRIVATEPROPERTY_XREF_PROPERTY_UID] ON [SYNTO].[EDGE_ELEMENT_GLOBALPROPERTY_PRIVATEPROPERTY_XREF_PROPERTY] ([EDGE_ELEMENT_GLOBALPROPERTY_PRIVATEPROPERTY_XREF_PROPERTY_UID])
GO
CREATE UNIQUE INDEX [UX_EDGE_ELEMENT_GLOBALPROPERTY_PRIVATEPROPERTY_XREF_PROPERTY] ON [SYNTO].[EDGE_ELEMENT_GLOBALPROPERTY_PRIVATEPROPERTY_XREF_PROPERTY] ([EDGE_ELEMENT_GLOBALPROPERTY_PRIVATEPROPERTY_XREF_PROPERTY_UID], [EDGE_ELEMENT_GLOBALPROPERTY_PRIVATEPROPERTY_XREF_UID], [TYPE_UID_EDGE_PROPERTY])
GO
CREATE INDEX [IX_EDGE_ELEMENT_GLOBALPROPERTY_PRIVATEPROPERTY_XREF_PROPERTY_TYPE_UID_EDGE_PROPERTY] ON [SYNTO].[EDGE_ELEMENT_GLOBALPROPERTY_PRIVATEPROPERTY_XREF_PROPERTY]([TYPE_UID_EDGE_PROPERTY]);
GO
CREATE INDEX [IX_EDGE_ELEMENT_GLOBALPROPERTY_PRIVATEPROPERTY_XREF_PROPERTY_EDGE_ELEMENT_GLOBALPROPERTY_PRIVATEPROPERTY_XREF_UID] ON [SYNTO].[EDGE_ELEMENT_GLOBALPROPERTY_PRIVATEPROPERTY_XREF_PROPERTY]([EDGE_ELEMENT_GLOBALPROPERTY_PRIVATEPROPERTY_XREF_UID]);
GO
CREATE NONCLUSTERED INDEX [IX_EDGE_ELEMENT_GLOBALPROPERTY_PRIVATEPROPERTY_XREF_PROPERTY_TYPE_UID]
    ON [SYNTO].[EDGE_ELEMENT_GLOBALPROPERTY_PRIVATEPROPERTY_XREF_PROPERTY]([TYPE_UID_EDGE_PROPERTY] ASC);
GO