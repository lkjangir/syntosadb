﻿CREATE VIEW [SYNTO].[VW_EDGE_ELEMENT_GLOBALPROPERTY_XREF_PROPERTY]
AS 
	SELECT   A.*
			,B.[TYPE_NAME] AS [TYPE_NAME_EDGE_PROPERTY]
			,B.[ISACTIVE] AS [ISACTIVE_TYPE_EDGE]
			,C.[ELEMENT_GLOBALPROPERTY_UID]
			,C.[ELEMENT_UID]
			,C.ELEMENT_UID_DRIVER
			,E.[ISACTIVE] AS [ISACTIVE_ELEMENT]
			,E.[ELEMENT_NAME]
			,E.[TYPE_UID] AS [TYPE_UID_ELEMENT]
			,G.[ISACTIVE] AS [ISACTIVE_ELEMENT_TYPE]
			,G.[TYPE_NAME] AS [TYPE_NAME_ELEMENT]
			,F.[TYPE_UID] AS [TYPE_UID_GLOBALPROPERTY]
			,F.[GLOBAL_ATTRIBUTE]
			,H.[TYPE_NAME] AS [TYPE_NAME_GLOBALPROPERTY]
			,I.[ELEMENT_NAME] AS [ELEMENT_NAME_GLOBALPROPERTY]
			,I.[ISACTIVE] AS [ISACTIVE_GLOBALPROPERTY_ELEMENT]
			,I.[TYPE_UID] AS [TYPE_UID_GLOBALPROPERTY_ELEMENT]
	  FROM [SYNTO].[EDGE_ELEMENT_GLOBALPROPERTY_XREF_PROPERTY] A
	 INNER JOIN [SYNTO].[TYPE] B
	    ON A.[TYPE_UID_EDGE_PROPERTY] = B.[TYPE_UID]
	 INNER JOIN [SYNTO].[EDGE_ELEMENT_GLOBALPROPERTY_XREF] C
	    ON A.[EDGE_ELEMENT_GLOBALPROPERTY_XREF_UID] = C.[EDGE_ELEMENT_GLOBALPROPERTY_XREF_UID]
	 INNER JOIN  [SYNTO].[TYPE] D
		ON C.[TYPE_UID_EDGE] = D.[TYPE_UID] 
	 INNER JOIN [SYNTO].[ELEMENT] E
		ON C.[ELEMENT_UID] = E.[ELEMENT_UID]
	 INNER JOIN [SYNTO].[ELEMENT_GLOBALPROPERTY] F
		ON C.[ELEMENT_GLOBALPROPERTY_UID] = F.[ELEMENT_GLOBALPROPERTY_UID]
	 INNER JOIN [SYNTO].[TYPE] G
	    ON E.[TYPE_UID] = G.[TYPE_UID]
	 INNER JOIN [SYNTO].[TYPE] H
	    ON F.[TYPE_UID] = H.[TYPE_UID]
	 INNER JOIN [SYNTO].[ELEMENT] I
	    ON F.[ELEMENT_UID] = I.[ELEMENT_UID]